package com.example.kenakataadmin;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.databinding.ActivityShopListBinding;
import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.models.Shop;
import com.example.kenakataadmin.services.ApiService;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopListActivity extends AppCompatActivity {
    private ActivityShopListBinding binding;
    private ApiService allDetailsApiService;
    private String url2;
    int id;
    TextView shopListCLick;
    TextView shopListCLickM;
    SearchView searchView;
    FloatingActionButton addShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop_list);
        shopListCLick = findViewById(R.id.searchMenuIconTV);
        shopListCLickM = findViewById(R.id.shopListTV);
        searchView = findViewById(R.id.searchlocationSV);
        addShop=findViewById(R.id.addShopFAB);

        addShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ShopListActivity.this, ShopRegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        });

        shopListCLick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopListCLickM.setVisibility(View.INVISIBLE);
                searchView.setVisibility(View.VISIBLE);
                shopListCLick.setVisibility(View.INVISIBLE);
            }
        });


       // hideSearchViewIcon();
        getAllShop();

    }        

    private void getAllShop() {

        url2 = String.format("api/FindShopkeepers/Shopkeepers");
        allDetailsApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        Call<List<Shop>> call = allDetailsApiService.getAllShops(url2);
        call.enqueue(new Callback<List<Shop>>() {
            @Override
            public void onResponse(Call<List<Shop>> call, final Response<List<Shop>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(ShopListActivity.this, "Failed and Response Code: " + response.code(), Toast.LENGTH_SHORT).show();
                } else {

                    final List<Shop> shops = response.body();


                    class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopViewHolder> implements Filterable {


                        List<Shop> shopFull = response.body();
                        List<Shop> shopList = response.body();

                        @NonNull
                        @Override
                        public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroupShop, int i) {
                            View v = LayoutInflater.from(viewGroupShop.getContext())
                                    .inflate(R.layout.model_shop_item,viewGroupShop,false);


                            return new ShopViewHolder(v);
                        }

                        @Override
                        public void onBindViewHolder(@NonNull final ShopViewHolder shopViewHolder, int i) {
                            final Shop shop = shopList.get(i);
                            shopViewHolder.shopNameList.setText(shop.getShopName());
                            shopViewHolder.shopAddressList.setText(shop.getFullAddress());
                            try{

                                String url = shop.getImageURL();


                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                Bitmap bitmap = (Bitmap) BitmapFactory.decodeFile(url);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                byte[] imageBytes = baos.toByteArray();
                                String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                                //decode base64 string to image
                                imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                                shopViewHolder.circleImageView.setImageBitmap(decodedImage);

                              // Picasso.get().load(bitmap).into(shopViewHolder.circleImageView);*/
                            }catch(Exception e){
                                e.getMessage();
                            }










                            shopViewHolder.linearLayoutForDetails.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intentDetails = new Intent(ShopListActivity.this,DetailsActiviy.class);

                                    intentDetails.putExtra("shopID", shop.getId());
                                    intentDetails.putExtra("shopName", shop.getShopName());
                                    intentDetails.putExtra("shopAddress", shop.getFullAddress());
                                    intentDetails.putExtra("shopOwnerName", shop.getOwnerName());
                                    intentDetails.putExtra("phoneNumber", shop.getPhone());
                                    intentDetails.putExtra("latitude", shop.getLatitude());
                                    intentDetails.putExtra("longitude", shop.getLongitude());
                                    intentDetails.putExtra("ShopImage", shop.getImageURL());
                                    startActivity(intentDetails);

                                }
                            });





                            shopViewHolder.menu.setOnClickListener(new View.OnClickListener() {


                                @Override
                            public void onClick(View v) {
                                PopupMenu popup = new PopupMenu(ShopListActivity.this, shopViewHolder.menu);
                                popup.getMenuInflater().inflate(R.menu.menu_action, popup.getMenu());

                                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    public boolean onMenuItemClick(MenuItem item) {
                                        switch (item.getItemId()) {
                                            case R.id.editItem:
                                                int shopRegId = shop.getId();
                                                String shopNameReg = shop.getShopName();
                                                String shopAddressReg = shop.getFullAddress();
                                                String shopOwnerNameReg = shop.getOwnerName();
                                                String phoneNumberReg = shop.getPhone();
                                                String emailReg = shop.getEmail();
                                                String shopTypeReg = shop.getShopType();
                                                String shopImageReg = shop.getImageURL();
                                                double latShopReg = shop.getLatitude();
                                                double lonShopReg = shop.getLongitude();
                                                Intent intent=new Intent(ShopListActivity.this,EditShopActivity.class);

                                                intent.putExtra("shopRegID", shopRegId);
                                                intent.putExtra("shopNameREG", shopNameReg);
                                                intent.putExtra("shopAddressREG", shopAddressReg);
                                                intent.putExtra("shopOwnerNameREG", shopOwnerNameReg);
                                                intent.putExtra("phoneNumberREG", phoneNumberReg);
                                                intent.putExtra("emailREG", emailReg);
                                                intent.putExtra("shopTypeREG", shopTypeReg);
                                                intent.putExtra("shopImageREG", shopImageReg);
                                                intent.putExtra("shopLat", latShopReg);
                                                intent.putExtra("shopLon", lonShopReg);

                                                startActivity(intent);

                                                break;

                                            case R.id.deleteItem:
                                                AlertDialog.Builder builder = new AlertDialog.Builder(ShopListActivity.this);
                                                builder.setMessage("Are you sure??")
                                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                deletePost(shop.getId());

                                                            }
                                                        }).setNegativeButton("Cancel",null);
                                                AlertDialog alertDialog = builder.create();
                                                alertDialog.show();


                                                break;

                                            default:
                                                break;
                                        }
                                        return false;

                                    }
                                });

                                popup.show();
                            }
                        });


                        }

                        @Override
                        public int getItemCount() {
                            return shopList.size();
                        }

                        @Override
                        public Filter getFilter() {
                            return shopFilter;
                        }
                        private Filter shopFilter = new Filter() {
                            @Override
                            protected FilterResults performFiltering(CharSequence charSequence) {
                                List<Shop> filterShop = new ArrayList<>();
                                if (charSequence == null || charSequence.length() == 0){
                                    filterShop.addAll(shopFull);
                                }
                                else {
                                    String filterPattern = charSequence.toString().toLowerCase().trim();

                                    for (Shop shop : shopFull){
                                        if (shop.getShopName().toLowerCase().contains(filterPattern)){
                                            filterShop.add(shop);
                                        }
                                    }
                                }
                                FilterResults results = new FilterResults();
                                results.values = filterShop;
                                return results;
                            }

                            @Override
                            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                                shopList.clear();
                                shopList.addAll((List) filterResults.values);
                                notifyDataSetChanged();

                            }
                        };

                        class ShopViewHolder extends RecyclerView.ViewHolder{

                            TextView shopNameList;
                            TextView shopAddressList;
                            CircleImageView circleImageView;
                            TextView menu;
                            LinearLayout linearLayoutForDetails;


                            public ShopViewHolder(@NonNull final View itemView) {
                                super(itemView);
                                shopNameList = itemView.findViewById(R.id.shopNameListTV);
                                shopAddressList = itemView.findViewById(R.id.shopAddressListTV);
                                circleImageView = itemView.findViewById(R.id.profile_imageCIV);
                                menu=itemView.findViewById(R.id.menuBtn);
                                linearLayoutForDetails = itemView.findViewById(R.id.linearLayoutForDetailsActivity);


                            }

                        }
                        ShopAdapter(List<Shop> shopList){
                            this.shopList = shopList;
                            shopFull = new ArrayList<>(shopList);
                        }
                    }

                    final ShopAdapter adapter;
                    RecyclerView recyclerView = findViewById(R.id.shopNameRecyclerView);
                    recyclerView.setHasFixedSize(true);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ShopListActivity.this);
                    adapter = new ShopAdapter(shops);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);

                    binding.searchlocationSV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String s) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String s) {
                            if (s.isEmpty()){
                                shops.clear();
                                adapter.notifyDataSetChanged();
                            }
                            adapter.getFilter().filter(s);
                            return true;
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<List<Shop>> call, Throwable t) {

            }
        });


    }

    private void hideSearchViewIcon() {
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) binding.searchlocationSV.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        magImage.setVisibility(View.GONE);
    }

    private void deletePost(int id){

        ApiService deleteApiService;
        deleteApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        Call<Void> call = deleteApiService.deletePost(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response){
                if (!response.isSuccessful()){
                    Toast.makeText(ShopListActivity.this, "Wrong format found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ShopListActivity.this, "Deleted !!!"+response.toString(), Toast.LENGTH_SHORT).show();
                    refreshActivity();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(ShopListActivity.this, "Failure", Toast.LENGTH_SHORT).show();

            }
        });

    }
    private void refreshActivity(){
        Intent refresh = new Intent(ShopListActivity.this, ShopListActivity.class);
        startActivity(refresh);
        finish();
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }
}
