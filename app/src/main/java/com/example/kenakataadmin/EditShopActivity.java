package com.example.kenakataadmin;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.models.Shop;
import com.example.kenakataadmin.services.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditShopActivity extends AppCompatActivity {

    EditText shopName, shopAddress, shopOwnerName, shopPhoneNumber, shopEmailAddress, shopType, shopLat, shopLon, shopImageUrl;

    private Button update;
    private ApiService updateApiService;


    private int valueIdReg;
    private String valueShopNameReg;
    private String valueShopAddressReg;
    private String valueOwnerNameReg;
    private String valuePhoneNumberReg;
    private String valueEmailAddressReg;
    private String valueShopTypeReg;
    private double valueLatReg;
    private double valueLonReg;
    private String valueImageUrlReg;
    private TextView updateConfirmed;
    private Snackbar snackbarUpdate;
    private static final String DATA_UPDATED = "Your data is updated now";


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            Intent i = new Intent(this, ShopListActivity.class);
            startActivity(i);
            snackbarUpdate.dismiss();
            finish();
        }
        return super.onKeyDown(keyCode,event);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shop);
        updateConfirmed = findViewById(R.id.responseEditTV);
        update = findViewById(R.id.submitBtnEdit);
        shopName = findViewById(R.id.shopNameEditET);
        shopAddress = findViewById(R.id.shopAddressEditET);
        shopOwnerName = findViewById(R.id.ownerNameEditET);
        shopPhoneNumber = findViewById(R.id.phoneNumberEditET);
        shopEmailAddress = findViewById(R.id.emailEditET);
        shopType = findViewById(R.id.shopTypeEditET);
        shopLat = findViewById(R.id.latitudeEditET);
        shopLon = findViewById(R.id.longitudeEditET);
        shopImageUrl = findViewById(R.id.ImageUrlEditET);

        updateApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);

        Bundle getExtra = getIntent().getExtras();
        if (getExtra != null) {
            valueIdReg = getExtra.getInt("shopRegID");
            valueShopNameReg = getExtra.getString("shopNameREG");
            valueShopAddressReg = getExtra.getString("shopAddressREG");
            valueOwnerNameReg = getExtra.getString("shopOwnerNameREG");
            valuePhoneNumberReg = getExtra.getString("phoneNumberREG");
            valueEmailAddressReg = getExtra.getString("emailREG");
            valueShopTypeReg = getExtra.getString("shopTypeREG");
            valueLatReg = getExtra.getDouble("shopLat");
            valueLonReg = getExtra.getDouble("shopLon");
            valueImageUrlReg = getExtra.getString("shopImageREG");
        }
        shopName.setText(valueShopNameReg);
        shopAddress.setText(valueShopAddressReg);
        shopOwnerName.setText(valueOwnerNameReg);
        shopPhoneNumber.setText(valuePhoneNumberReg);
        shopEmailAddress.setText(valueEmailAddressReg);
        shopType.setText(valueShopTypeReg);
        shopLat.setText(String.valueOf(valueLatReg));
        shopLon.setText(String.valueOf(valueLonReg));
        shopImageUrl.setText(valueImageUrlReg);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shopname = shopName.getText().toString();
                String shopaddress = shopAddress.getText().toString();
                String shopownername = shopOwnerName.getText().toString();
                String shophonenumber = shopPhoneNumber.getText().toString();
                String shopemailaddress = shopEmailAddress.getText().toString();
                String shoptype = shopType.getText().toString();
                String shoplat = shopLat.getText().toString();
                String shoplon = shopLon.getText().toString();
                String shopimageurl = shopImageUrl.getText().toString();

                if (shopname.equals(valueShopNameReg)&&
                        shopaddress.equals(valueShopAddressReg) &&
                        shopownername.equals(valueOwnerNameReg) &&
                        shophonenumber.equals(valuePhoneNumberReg) &&
                        shopemailaddress.equals(valueEmailAddressReg) &&
                        shoptype.equals(valueShopTypeReg) &&
                        shoplat.equals(String.valueOf(valueLatReg)) &&
                        shoplon.equals(String.valueOf(valueLonReg)) &&
                        shopimageurl.equals(valueImageUrlReg)){

                    Toast.makeText(EditShopActivity.this, "No changed, You can't update", Toast.LENGTH_SHORT).show();
                }
                else {
                    double latValue,lonValue;
                    latValue = Double.valueOf(shoplat);
                    lonValue = Double.valueOf(shoplon);
                    updatePost(valueIdReg,shopname,shoptype,shopaddress,shopownername,latValue,lonValue,shopimageurl,shophonenumber,
                            shopemailaddress);
                }
            }
        });
    }

    public void updatePost(Integer id, String shopName, String shopType,
                           String fullAddress, String ownerName,
                           Double latitude, Double longitude, String imageURL, String phone, String email) {

        final Shop shop = new Shop(id,shopName, shopType, fullAddress, ownerName, latitude, longitude,imageURL,phone,email);
        Call<Shop> call1 = updateApiService.updatePost(valueIdReg,shop);
        call1.enqueue(new Callback<Shop>() {
            @Override
            public void onResponse(Call<Shop> call, Response<Shop> response) {
                if (response.isSuccessful()) {
                    showResponse();
                } else {
                    Toast.makeText(EditShopActivity.this, "Wrong format found", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<Shop> call, Throwable t) {
                Toast.makeText(EditShopActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void showResponse() {
        snackbarUpdate = Snackbar.make(updateConfirmed, "" + DATA_UPDATED, Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(EditShopActivity.this, ShopListActivity.class);
                        startActivity(i);
                        snackbarUpdate.dismiss();
                        finish();

                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbarUpdate.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbarUpdate.show();
    }


}
