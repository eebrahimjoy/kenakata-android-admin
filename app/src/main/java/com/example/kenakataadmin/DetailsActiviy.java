package com.example.kenakataadmin;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.models.Discount;
import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.services.ApiService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActiviy extends AppCompatActivity {
    private static final int EDIT_OK = 2;
    private static final int INACTIVE_REQUEST_OK = 1;

    String urlDiscount;
    ApiService apiServiceDiscountList;
    FloatingActionButton addDiscountBTN;
    ApiService deleteDiscountService;
    TextView shopNameDtails, shopAddressDetails, ownerNameDetails, phoneNumber;
    int valueIdDetails;
    String valueShopNameDetails, valueShopAddressDetails, valueShopOwnerNameDetails, valuePhoneNumberDetails;
    double latitudeDetails, longitudeDetails;
    String urlShopImage;

    ImageView shopImageViewDetails;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == INACTIVE_REQUEST_OK){
            if (resultCode == Activity.RESULT_OK){
                int idgot = data.getIntExtra("IdReturn",0);
                getDiscountData(idgot);
            }
        }
        if (requestCode == EDIT_OK){
            if (resultCode == Activity.RESULT_OK){
                int idget = data.getIntExtra("ShopkeeperIdReturn",0);
                getDiscountData(idget);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(this, ShopListActivity.class);
            startActivity(i);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_activiy);;
        shopNameDtails = findViewById(R.id.shopNameDetailsTV);
        shopAddressDetails = findViewById(R.id.shopAddressDetailsTV);
        ownerNameDetails = findViewById(R.id.ownerNameDetailsTV);
        phoneNumber = findViewById(R.id.phoneNumberTV);
        shopImageViewDetails = findViewById(R.id.detailsImageIV);


        addDiscountBTN = findViewById(R.id.addDiscountFAB);

        Bundle getExtraDetails = getIntent().getExtras();
        if (getExtraDetails != null) {
            valueIdDetails = getExtraDetails.getInt("shopID");
            valueShopNameDetails = getExtraDetails.getString("shopName");
            valueShopAddressDetails = getExtraDetails.getString("shopAddress");
            valueShopOwnerNameDetails = getExtraDetails.getString("shopOwnerName");
            valuePhoneNumberDetails = getExtraDetails.getString("phoneNumber");
            latitudeDetails = getExtraDetails.getDouble("latitude");
            longitudeDetails = getExtraDetails.getDouble("longitude");
            urlShopImage = getExtraDetails.getString("ShopImage");
        }
        getDiscountData(valueIdDetails);

        shopNameDtails.setText(valueShopNameDetails);
        shopAddressDetails.setText(valueShopAddressDetails);
        ownerNameDetails.setText(valueShopOwnerNameDetails);
        phoneNumber.setText(valuePhoneNumberDetails);
        //Picasso.get().load(urlShopImage).into(shopImageViewDetails);


        addDiscountBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDiscount = new Intent(DetailsActiviy.this, DiscountFormActivity.class);
                intentDiscount.putExtra("ID", valueIdDetails);
                intentDiscount.putExtra("Lat", latitudeDetails);
                intentDiscount.putExtra("Lon", longitudeDetails);

                startActivityForResult(intentDiscount,INACTIVE_REQUEST_OK);
            }
        });


    }



        private void getDiscountData(int id) {

        urlDiscount = String.format("api/Discounts/AllDiscounts?id=%d",id);
        apiServiceDiscountList = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        final Call<List<Discount>> discountCall = apiServiceDiscountList.getAllDiscount(urlDiscount);
        discountCall.enqueue(new Callback<List<Discount>>() {
            @Override
            public void onResponse(Call<List<Discount>> call, final Response<List<Discount>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(DetailsActiviy.this, "Wrong format found test" + response.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    final List<Discount> addDiscounts = response.body();

                    class DiscountAdapter extends RecyclerView.Adapter<DiscountAdapter.DiscountViewHolder> {

                        List<Discount> discounts = response.body();

                        DiscountAdapter(List<Discount> discounts) {
                            this.discounts = discounts;
                        }


                        @NonNull
                        @Override
                        public DiscountViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroupDis, int i) {

                            LayoutInflater inflater = LayoutInflater.from(viewGroupDis.getContext());
                            View viewDis = inflater.inflate(R.layout.model_discount_list, viewGroupDis, false);

                            return new DiscountViewHolder(viewDis);
                        }

                        @Override
                        public void onBindViewHolder(@NonNull final DiscountViewHolder discountViewHolder, int position) {

                            final Discount addDiscount = discounts.get(position);
                            int discountCheck = addDiscount.getDiscountAmount();
                            String discountAmnt;
                            discountAmnt = String.valueOf(discountCheck);
                            discountViewHolder.discountAmountCheck.setText(discountAmnt);
                            Long dateFrom = addDiscount.getFromDate();
                            Long dateTo = addDiscount.getToDate();

                            Date fromDate = new Date(dateFrom);
                            SimpleDateFormat fromDateInDateFormat = new SimpleDateFormat("dd MMM yyy");
                            final String fromDateString = fromDateInDateFormat.format(fromDate);

                            Date toDate = new Date(dateTo);
                            SimpleDateFormat toDateInDateFormat = new SimpleDateFormat("dd MMM yyy");
                            final String toDateString = toDateInDateFormat.format(toDate);

                            discountViewHolder.fromDate.setText(fromDateString);
                            discountViewHolder.toDate.setText(toDateString);

                            boolean statusActive;
                            statusActive = addDiscount.getIsActive();
                            if (statusActive == true) {
                                discountViewHolder.activeStatus.setText("Active");
                            } else {
                                discountViewHolder.activeStatus.setText("In-active");
                            }

                            discountViewHolder.discountMenu.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    PopupMenu popup = new PopupMenu(DetailsActiviy.this, discountViewHolder.discountMenu);
                                    popup.getMenuInflater().inflate(R.menu.discount_menu, popup.getMenu());
                                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        public boolean onMenuItemClick(MenuItem item) {
                                            switch (item.getItemId()) {
                                                case R.id.editItemDiscount:
                                                    int ediTime = 1;
                                                    Intent editInent = new Intent(DetailsActiviy.this,EditDiscountActivity.class);
                                                    editInent.putExtra("Discount",addDiscount.getDiscountAmount());
                                                    editInent.putExtra("FromDate",fromDateString);
                                                    editInent.putExtra("ToDate",toDateString);
                                                    editInent.putExtra("ActiveStatus",addDiscount.getIsActive());
                                                    editInent.putExtra("IdEdit",addDiscount.getId());
                                                    editInent.putExtra("Flag",ediTime);
                                                    editInent.putExtra("ShopKeeperID",addDiscount.getShopkeeperId());
                                                    editInent.putExtra("LatEdit",addDiscount.getLatitude());
                                                    editInent.putExtra("LongEdit",addDiscount.getLongitude());
                                                   startActivityForResult(editInent,EDIT_OK);
                                                    break;

                                                case R.id.deleteItemDiscount:
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailsActiviy.this);
                                                    builder.setMessage("Are you sure??")
                                                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                                    deleteDiscountPost(addDiscount.getId());

                                                                }
                                                            }).setNegativeButton("Cancel", null);
                                                    AlertDialog alertDialog = builder.create();
                                                    alertDialog.show();


                                                    break;

                                                default:
                                                    break;

                                            }
                                            return true;
                                        }
                                    });

                                    popup.show();

                                }
                            });


                        }

                        @Override
                        public int getItemCount() {
                            return discounts.size();
                        }

                        class DiscountViewHolder extends RecyclerView.ViewHolder {
                            TextView discountAmountCheck, fromDate, toDate;
                            Button activeStatus;
                            TextView discountMenu;

                            public DiscountViewHolder(@NonNull View itemView) {
                                super(itemView);
                                discountAmountCheck = itemView.findViewById(R.id.discountAmountDiscount);
                                fromDate = itemView.findViewById(R.id.fromDateDiscount);
                                toDate = itemView.findViewById(R.id.toDateDiscount);
                                activeStatus = itemView.findViewById(R.id.activStatusBtn);
                                discountMenu = itemView.findViewById(R.id.discountMenuTV);

                            }
                        }

                    }
                    final DiscountAdapter adapterDis;
                    RecyclerView recyclerViewDis = findViewById(R.id.discountListRecyclerView);
                    recyclerViewDis.setHasFixedSize(true);
                    RecyclerView.LayoutManager layoutManagerDis = new LinearLayoutManager(DetailsActiviy.this);
                    adapterDis = new DiscountAdapter(addDiscounts);
                    recyclerViewDis.setLayoutManager(layoutManagerDis);
                    recyclerViewDis.setAdapter(adapterDis);
                }
            }

            @Override
            public void onFailure(Call<List<Discount>> call, Throwable t) {

                Toast.makeText(DetailsActiviy.this, "On Failure" + t.toString() + "\n" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void deleteDiscountPost(int id) {

        deleteDiscountService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        Call<Void> call = deleteDiscountService.deleteDiscount(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(DetailsActiviy.this, "Wrong format found", Toast.LENGTH_SHORT).show();
                } else {
                    getDiscountData(valueIdDetails);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(DetailsActiviy.this, "Failure", Toast.LENGTH_SHORT).show();

            }
        });

    }

}
