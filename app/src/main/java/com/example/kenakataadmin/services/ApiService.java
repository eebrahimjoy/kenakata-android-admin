package com.example.kenakataadmin.services;

import com.example.kenakataadmin.PlaceAPI.Predictions;
import com.example.kenakataadmin.models.Discount;
import com.example.kenakataadmin.models.Shop;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {

    @GET("place/autocomplete/json")
    Call<Predictions> getPlacesAutoComplete(
            @Query("input") String input,
            @Query("types") String types,
            @Query("location") String location,
            @Query("radius") String radius,
            @Query("key") String key
    );

    @FormUrlEncoded
    @POST("token")
    Call<ResponseBody> sendCredentials(
            @Field("grant_type") String grant_type,
            @Field("UserName") String UserName,
            @Field("Password") String Password
    );

    @POST("api/Shopkeepers/CreateShopkeeper")
    Call<Shop> savePost(@Body Shop responsePost);

    @POST("api/Discounts/CreateDiscount")
    Call<Discount> addDiscountPost(@Body Discount responseDiscountPost);

    @PUT("api/Shopkeepers/EditShopkeeper/{id}")
    Call<Shop> updatePost(@Path("id") int id, @Body Shop UpdatePost);

    @PUT("api/Discounts/EditDiscount/{id}")
    Call<Discount> editDiscount(@Path("id") int id, @Body Discount editDiscount);

    @PUT("api/Discounts/EditDiscount/{id}")
    Call<Discount> editActiveDiscount(@Path("id") int id, @Body Discount editActiveDiscount);

    @DELETE("api/Shopkeepers/DeleteShopkeeper/{id}")
    Call<Void> deletePost(@Path("id") int id);

    @DELETE("api/Discounts/DeleteDiscount/{id}")
    Call<Void> deleteDiscount(@Path("id") int id);

    @GET()
    Call<List<Shop>> getAllShops(@Url String url);

    @GET()
    Call<List<Discount>> getAllDiscount(@Url String url);


}