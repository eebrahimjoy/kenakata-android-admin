package com.example.kenakataadmin;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.models.Shop;
import com.example.kenakataadmin.services.ApiService;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopRegistrationActivity extends AppCompatActivity {

    private Snackbar snackbar;
    private static final String DATA_SAVED = " Your data is saved successfully";
    Double searchLat,searchLon;
    String searchAddress;
    TextView image;
    Button placePickerr;
    private  String temp,shpName,finalShopName;
    String path;
    private static final int RESULT_CODE = 11;
    private ImageView regImageview;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;


    String shopNameText;
    String shopAddressText;
    String shopTypeText;
    String ownerNameText;
    String phoneNumberText;
    String emailText;

    EditText shopName,  ownerName, phoneNumber, email, shopType, latitude,  longitude, imageUrl;
    Button submit;
    ApiService apiServicePost;

    TextView responseText,shopAddress;

    TextView textViewBtn;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            Intent i = new Intent(this, ShopListActivity.class);
            startActivity(i);
            snackbar.dismiss();
            finish();
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if(requestCode==RESULT_CODE){
            if(resultCode==RESULT_OK){

                searchLat=data.getDoubleExtra("latitude",0);
                searchLon=data.getDoubleExtra("longitude",0);
                searchAddress=data.getStringExtra("address");
                shopAddress.setText(searchAddress);
                placePickerr.setVisibility(View.GONE);

                *//*sendPost(shopNameText, shopTypeText, shopAddressText, ownerNameText,
                        searchLat,searchLon, path, phoneNumberText, emailText);

                shopName.setText("");
                shopAddress.setText("");
                shopType.setText("");
                ownerName.setText("");
                phoneNumber.setText("");
                email.setText("");
                latitude.setText("");
                longitude.setText("");
*//*

            }
        }
*/


        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    path = saveImage(bitmap);
                    Toast.makeText(ShopRegistrationActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    regImageview.setImageBitmap(bitmap);
                    BitMapToString(bitmap);
                    Toast.makeText(ShopRegistrationActivity.this, ""+path, Toast.LENGTH_LONG).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(ShopRegistrationActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            regImageview.setImageBitmap(thumbnail);
            path=saveImage(thumbnail);
            Toast.makeText(ShopRegistrationActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
            regImageview.setImageBitmap(thumbnail);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_registration);
        requestMultiplePermissions();



       /* textViewBtn = findViewById(R.id.submitBtnReg);*/

       /* textViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ShopRegistrationActivity.this,ShopListActivity.class);
                startActivity(i);
            }
        });*/













        shopName = findViewById(R.id.shopNameRegET);
        shopAddress = findViewById(R.id.shopAddressRegET);
        ownerName = findViewById(R.id.ownerNameRegET);
        phoneNumber = findViewById(R.id.phoneNumberRegET);
        email = findViewById(R.id.emailRegET);
        shopType = findViewById(R.id.shopTypeRegET);
        textViewBtn = findViewById(R.id.submitBtnReg);
        responseText = findViewById(R.id.responseRegTV);
        image=findViewById(R.id.ImageUrlRegET);
        regImageview=findViewById(R.id.regImageIV);
        placePickerr = findViewById(R.id.placepickerBTN);
        apiServicePost = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);


        Bundle getExtraShop = getIntent().getExtras();
        if (getExtraShop != null) {
            searchLat = getExtraShop.getDouble("latitude");
            searchLon = getExtraShop.getDouble("longitude");
            searchAddress = getExtraShop.getString("address");
            shpName = getExtraShop.getString("shopGet");
            shopName.setText(shpName);
            placePickerr.setVisibility(View.GONE);
        }
        shopAddress.setText(searchAddress);


        Bundle getExtra = getIntent().getExtras();
        if (getExtraShop != null) {
            searchLat = getExtra.getDouble("latitude");
            searchLon =getExtra.getDouble("longitude");
            searchAddress = getExtra.getString("address");
            shpName = getExtra.getString("shopGet");
            shopName.setText(shpName);
            placePickerr.setVisibility(View.GONE);
        }
        shopAddress.setText(searchAddress);







       image.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               showPictureDialog();

           }
       });

        placePickerr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopNameText = shopName.getText().toString();
                Intent mapIntent = new Intent(ShopRegistrationActivity.this,MapsActivity.class);
                mapIntent.putExtra("ShopStoragValue",shopNameText);
                mapIntent.putExtra("valueforAdd",100);
                startActivity(mapIntent);


            }
        });



      /*  username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag!=0){
                    snackbar.dismiss();
                }
            }
        });

        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag==1){
                    snackbar.dismiss();
                }
            }
        });*/

        textViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                finalShopName = shopName.getText().toString();
                 shopAddressText = shopAddress.getText().toString();
                 shopTypeText = shopType.getText().toString();
                 ownerNameText = ownerName.getText().toString();
                 phoneNumberText = phoneNumber.getText().toString();
                 emailText = email.getText().toString();

                if (finalShopName.isEmpty() && shopAddressText.isEmpty()
                        && shopTypeText.isEmpty() && ownerNameText.isEmpty()
                        && phoneNumberText.isEmpty() && emailText.isEmpty()
                        && path.isEmpty())  {
                    Toast.makeText(ShopRegistrationActivity.this, "First fill-up the form", Toast.LENGTH_SHORT).show();

                } else if (finalShopName.isEmpty() || shopAddressText.isEmpty()
                        || shopTypeText.isEmpty() || ownerNameText.isEmpty()
                        || phoneNumberText.isEmpty() || emailText.isEmpty()
                        || path.isEmpty()) {
                    if (finalShopName.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the shop name", Toast.LENGTH_SHORT).show();
                    }
                    if (shopAddressText.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the shop address", Toast.LENGTH_SHORT).show();
                    }

                    if (shopTypeText.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the shop type", Toast.LENGTH_SHORT).show();
                    }
                    if (ownerNameText.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the shop owner name", Toast.LENGTH_SHORT).show();
                    }

                    if (phoneNumberText.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the phone number", Toast.LENGTH_SHORT).show();
                    }
                    if (emailText.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the email address", Toast.LENGTH_SHORT).show();
                    }
                    if (path.isEmpty()) {
                        Toast.makeText(ShopRegistrationActivity.this, "First enter the imageUrl", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!finalShopName.isEmpty() && !shopAddressText.isEmpty()
                        && !shopTypeText.isEmpty() && !ownerNameText.isEmpty()
                        && !phoneNumberText.isEmpty() && !emailText.isEmpty()
                        && !path.isEmpty()) {



                    sendPost(finalShopName, shopTypeText, shopAddressText, ownerNameText,
                            searchLat,searchLon, path, phoneNumberText, emailText);

                    /*shopName.setText("");
                    shopAddress.setText("");
                    shopType.setText("");
                    ownerName.setText("");
                    phoneNumber.setText("");
                    email.setText("");
*/


                }

            }
        });
    }

    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }

    private void showPictureDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();


    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }
    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }




    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }



    public void sendPost(String shopName, String shopType, String fullAddress,
                         String ownerName, Double latitude,
                         Double longitude, String imageURL, String phone, String email) {

        final Shop shop = new Shop(shopName, shopType, fullAddress, ownerName, latitude, longitude,imageURL,phone,email);
        Call<Shop> call1 = apiServicePost.savePost(shop);
        call1.enqueue(new Callback<Shop>() {
            @Override
            public void onResponse(Call<Shop> call, Response<Shop> response) {
                if (response.isSuccessful()) {
                    showResponse();
                } else {
                    Toast.makeText(ShopRegistrationActivity.this, "Wrong format found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<Shop> call, Throwable t) {
                Toast.makeText(ShopRegistrationActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void showResponse() {
        snackbar = Snackbar.make(responseText, "" + DATA_SAVED, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(ShopRegistrationActivity.this, ShopListActivity.class);
                        startActivity(i);
                        snackbar.dismiss();
                        finish();

                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }



}
