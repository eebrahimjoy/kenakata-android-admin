package com.example.kenakataadmin;


import com.example.kenakataadmin.PlaceAPI.Prediction;

public interface PredictionInterface {
    void getPrediction(Prediction prediction);
}
