package com.example.kenakataadmin;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.services.ApiService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private int flag = 0;
    private Snackbar snackbar;
    private static final String YOU_ARE_SUCCESSFULLY_LOGGED_IN = " Your are successfully logged in";
    private static final String INCORRECT_CREDENTIAL = " Your username or password incorrect";

    EditText username;
    EditText password;
    ApiService apiServicePost;
    TextView responseText;
    Button logIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logIn=findViewById(R.id.loginBtnId);

        apiServicePost = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);


        username = findViewById(R.id.userNameET);
        password = findViewById(R.id.passwordET);
        responseText = findViewById(R.id.resposeTV);

       /* logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, DiscountFormActivity.class);
                startActivity(i);
                finish();

            }
        });*/
        //postData("Tariqul","123456");

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userNameText = username.getText().toString();
                String passwordText = password.getText().toString();

                if (userNameText.isEmpty() && passwordText.isEmpty()) {
                    Toast.makeText(MainActivity.this, "First fill-up the form", Toast.LENGTH_SHORT).show();
                } else if (userNameText.isEmpty() || passwordText.isEmpty()) {
                    if (userNameText.isEmpty()) {
                        Toast.makeText(MainActivity.this, "First enter the username", Toast.LENGTH_SHORT).show();
                    }
                    if (passwordText.isEmpty()) {
                        Toast.makeText(MainActivity.this, "First enter the password", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!userNameText.isEmpty() && !passwordText.isEmpty()) {
                    postData(userNameText, passwordText);
                    username.setText("");
                    password.setText("");

                }

            }
        });
    }


    public void postData(String UserName, String Password) {


        Call<ResponseBody> call1 = apiServicePost.sendCredentials("password", UserName, Password);
        call1.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, ""+YOU_ARE_SUCCESSFULLY_LOGGED_IN, Toast.LENGTH_SHORT).show();
                    gotoActivity();
                } else {
                    showResponse(INCORRECT_CREDENTIAL);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showResponse(t.getMessage());
                Toast.makeText(MainActivity.this, "Connect the device with internet"+t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        /*call1.enqueue(new Callback<UserCredential>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    //showResponse();
                    Toast.makeText(MainActivity.this, ""+response.code(), Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(MainActivity.this, "not"+response.code(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });
*/
    }

    public void showResponse(String message) {
        snackbar = Snackbar.make(responseText, "" + message, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void gotoActivity() {
        Intent i = new Intent(this, ShopListActivity.class);
        startActivity(i);
        finish();
    }


      /*  logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ShopRegistrationActivity.class));
            }
        });*/
    }

















































  /*  package com.example.kenakataadmin;

            import android.content.Intent;
            import android.graphics.Color;
            import android.os.Bundle;
            import android.support.design.widget.Snackbar;
            import android.support.v7.app.AppCompatActivity;
            import android.view.View;
            import android.widget.Button;
            import android.widget.EditText;
            import android.widget.TextView;
            import android.widget.Toast;

            import com.example.kenakataadmin.models.RetrofitInstance;
            import com.example.kenakataadmin.services.ApiService;

            import okhttp3.ResponseBody;
            import retrofit2.Call;
            import retrofit2.Callback;
            import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private int flag = 0;
    private Snackbar snackbar;
    private static final String YOU_ARE_SUCCESSFULLY_LOGGED_IN = " Your are successfully logged in";
    private static final String INCORRECT_CREDENTIAL = " Your username or password incorrect";

    EditText username;
    EditText password;
    Button login;
    ApiService apiServicePost;
    TextView responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiServicePost = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);


        username = findViewById(R.id.userNameET);
        password = findViewById(R.id.passwordET);
        login = findViewById(R.id.loginBTN);
        responseText = findViewById(R.id.resposeTV);
        //postData("Tariqul","123456");

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity();
               *//* String userNameText = username.getText().toString();
                String passwordText = password.getText().toString();*//*

                *//*if (userNameText.isEmpty() && passwordText.isEmpty()) {
                    Toast.makeText(MainActivity.this, "First fill-up the form", Toast.LENGTH_SHORT).show();
                } else if (userNameText.isEmpty() || passwordText.isEmpty()) {
                    if (userNameText.isEmpty()) {
                        Toast.makeText(MainActivity.this, "First enter the username", Toast.LENGTH_SHORT).show();
                    }
                    if (passwordText.isEmpty()) {
                        Toast.makeText(MainActivity.this, "First enter the password", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!userNameText.isEmpty() && !passwordText.isEmpty()) {
                    postData(userNameText, passwordText);
                    username.setText("");
                    password.setText("");

                }*//*

            }
        });
    }


    public void postData(String UserName, String Password) {


        Call<ResponseBody> call1 = apiServicePost.sendCredentials("password", UserName, Password);
        call1.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, ""+YOU_ARE_SUCCESSFULLY_LOGGED_IN, Toast.LENGTH_SHORT).show();
                    gotoActivity();
                } else {
                    showResponse(INCORRECT_CREDENTIAL);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showResponse(t.getMessage());
                Toast.makeText(MainActivity.this, "Connect the device with internet"+t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        *//*call1.enqueue(new Callback<UserCredential>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    //showResponse();
                    Toast.makeText(MainActivity.this, ""+response.code(), Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(MainActivity.this, "not"+response.code(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });
*//*
    }

    public void showResponse(String message) {
        snackbar = Snackbar.make(responseText, "" + message, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void gotoActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}*/

