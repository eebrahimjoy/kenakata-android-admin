package com.example.kenakataadmin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.models.Discount;
import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.services.ApiService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscountFormActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private boolean active;
    int discountIdForInactiveStatus = -1;
    int discountIdForPosition = 0;
    private int activeShopKeeperId;
    private int activeDiscount;
    double activeLat;
    double activeLot;
    long activeFromDate;
    long activeToDate;
    int flagInt= 0;
    private ApiService editApiService;
    private ApiService editActiveApiService;
    private boolean status;
    private double latitudeEdit, longitudeEdit;
    private int shopkeeperIdDiscount;
    private long fromDateEdit, toDateEdit;
    private int shopkeeperIdDiscountEdit;
    private Snackbar snackbar;
    String activeDiscountUrl;
    ApiService apiServiceAllDiscountList;
    String message;
    private static final int ADDED_DISCOUNT = 1;

    private static final String DISCOUNT_SAVED = " Discount added successfully";
    private static final String DISCOUNT_UPDATED = " Discount update successfully";
    private static final int UPDATED_DISCOUNT = 2;
    EditText addDiscountAmount;
    Button addDiscountBtn;

    private DatePickerDialog.OnDateSetListener dateSetListenerTo;
    private DatePickerDialog.OnDateSetListener dateSetListenerFrom;

    private long toDateInLong;
    private long fromDateInLong;
    ApiService apiServiceDiscountPost;
    TextView responseDiscount, fromDateDiscount, toDateDiscount;
    int idDiscount;
    double latDiscount, lonDiscount;
    private Spinner spinner;
    private int discountamount;

    int discountIdDiscountForm, flag = 0;
    int discountAmountDiscountForm;
    String fromDateDiscountForm;
    String toDateDiscountForm;
    boolean activeStatusDiscountForm;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("IdReturn",idDiscount);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
            snackbar.dismiss();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                active = true;
                break;
            case 1:
                active = false;
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discoun_form);


        addDiscountAmount = findViewById(R.id.addDiscountAmount);
        toDateDiscount = findViewById(R.id.addFromDate);
        fromDateDiscount = findViewById(R.id.addToDate);
        addDiscountBtn = findViewById(R.id.addAll);
        responseDiscount = findViewById(R.id.responseDisountTV);
        editApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);

        Bundle getExtraDiscount = getIntent().getExtras();
        if (getExtraDiscount != null) {
            idDiscount = getExtraDiscount.getInt("ID");
            latDiscount = getExtraDiscount.getDouble("Lat");
            lonDiscount = getExtraDiscount.getDouble("Lon");

        }

        apiServiceDiscountPost = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);

        toDateDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendarTo = Calendar.getInstance();
                int yearTo = calendarTo.get(Calendar.YEAR);
                int monthTo = calendarTo.get(Calendar.MONTH);
                int dayTo = calendarTo.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        DiscountFormActivity.this, AlertDialog.THEME_HOLO_LIGHT, dateSetListenerTo, yearTo, monthTo, dayTo);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        fromDateDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendarFrom = Calendar.getInstance();
                int yearFrom = calendarFrom.get(Calendar.YEAR);
                int monthFrom = calendarFrom.get(Calendar.MONTH);
                int dayFrom = calendarFrom.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        DiscountFormActivity.this, AlertDialog.THEME_HOLO_LIGHT, dateSetListenerFrom, yearFrom, monthFrom, dayFrom);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        dateSetListenerTo = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                String date = day + "/" + month + "/" + year;
                toDateDiscount.setText(date);


            }
        };

        dateSetListenerFrom = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                String date1 = day + "/" + month + "/" + year;
                fromDateDiscount.setText(date1);


            }
        };


        addDiscountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String discountAmount = addDiscountAmount.getText().toString();
                String toDate = toDateDiscount.getText().toString();
                String fromDate = fromDateDiscount.getText().toString();


                if (discountAmount.isEmpty() && toDate.isEmpty()
                        && fromDate.isEmpty()) {
                    Toast.makeText(DiscountFormActivity.this, "First fill-up the form", Toast.LENGTH_SHORT).show();
                } else if (discountAmount.isEmpty() || toDate.isEmpty()
                        || fromDate.isEmpty()) {
                    if (discountAmount.isEmpty()) {
                        Toast.makeText(DiscountFormActivity.this, "First enter the discount amount", Toast.LENGTH_SHORT).show();
                    }
                    if (toDate.isEmpty()) {
                        Toast.makeText(DiscountFormActivity.this, "First enter to date", Toast.LENGTH_SHORT).show();
                    }

                    if (fromDate.isEmpty()) {
                        Toast.makeText(DiscountFormActivity.this, "First enter from date", Toast.LENGTH_SHORT).show();
                    }


                }

                if (!discountAmount.isEmpty() && !toDate.isEmpty()
                        && !fromDate.isEmpty()) {


                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(toDate);
                        toDateInLong = date.getTime();


                        Date date1 = format.parse(fromDate);
                        fromDateInLong = date1.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    discountamount = Integer.valueOf(discountAmount);



                    getActiveDiscountStatus(idDiscount);

                   addDiscountAmount.setText("");
                    toDateDiscount.setText("");
                    fromDateDiscount.setText("");

                }


            }
        });
    }






    public void showResponseDiscount(int i) {
        if (i==ADDED_DISCOUNT){
            message = DISCOUNT_SAVED;
        }
        snackbar = Snackbar.make(responseDiscount, "" + message, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("IdReturn",idDiscount);
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


    public void getActiveDiscountStatus(int id) {
        activeDiscountUrl = String.format("api/Discounts/AllDiscounts?id=%d",id);
        editActiveApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        final Call<List<Discount>> discountCallActive = editActiveApiService.getAllDiscount(activeDiscountUrl);
        discountCallActive.enqueue(new Callback<List<Discount>>() {
            @Override
            public void onResponse(Call<List<Discount>> call, Response<List<Discount>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(DiscountFormActivity.this, "Wrong format found", Toast.LENGTH_SHORT).show();

                }
                else {
                    final List<Discount> allDiscountss = response.body();

                    if (allDiscountss.size() == 0){
                        sendDiscount(idDiscount, latDiscount, lonDiscount, discountamount,
                                toDateInLong, fromDateInLong, true);

                    }
                    else if (allDiscountss.size()>0){

                        for (int i = 0; i < allDiscountss.size(); i++) {
                            status = allDiscountss.get(i).getIsActive();
                            if (allDiscountss.get(i).getIsActive().equals(true)) {
                                flagInt = 1;
                                discountIdForPosition = i;

                                discountIdForInactiveStatus = allDiscountss.get(discountIdForPosition).getId();
                                activeShopKeeperId = allDiscountss.get(discountIdForPosition).getShopkeeperId();
                                activeDiscount = allDiscountss.get(discountIdForPosition).getDiscountAmount();
                                activeLat = allDiscountss.get(discountIdForPosition).getLatitude();
                                activeLot = allDiscountss.get(discountIdForPosition).getLongitude();
                                activeFromDate = allDiscountss.get(discountIdForPosition).getFromDate();
                                activeToDate = allDiscountss.get(discountIdForPosition).getToDate();

                                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(DiscountFormActivity.this);
                                builder.setMessage("Already an active discount found, are you deactivate it??")
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                inactiveStatus(discountIdForInactiveStatus,
                                                        activeShopKeeperId, activeLat,
                                                        activeLot, activeDiscount, activeFromDate,
                                                        activeToDate, false);


                                            }
                                        }).setNegativeButton("No", null);
                                android.support.v7.app.AlertDialog alertDialog = builder.create();
                                alertDialog.show();
                            }
                        }
                          if (flagInt == 0){
                            sendDiscount(idDiscount, latDiscount, lonDiscount, discountamount,
                                    toDateInLong, fromDateInLong, true);

                        }

                    }


                }
            }

            @Override
            public void onFailure(Call<List<Discount>> call, Throwable t) {
                Toast.makeText(DiscountFormActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void inactiveStatus(Integer activeDiscountId, Integer shopkeeperIdEdit, Double latitudeEdit,
                                Double longitudeEdit, Integer discountAmount,
                                Long fromDateEdit, Long toDateEdit, Boolean isActiveEdit) {
        editActiveApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        final Discount discountActive = new Discount(shopkeeperIdEdit, latitudeEdit, longitudeEdit,
                discountAmount, fromDateEdit, toDateEdit, isActiveEdit);
        Call<Discount> call1 = editActiveApiService.editActiveDiscount(activeDiscountId, discountActive);
        call1.enqueue(new Callback<Discount>() {
            @Override
            public void onResponse(Call<Discount> call, Response<Discount> response) {
                if (response.isSuccessful()) {
                    sendDiscount(idDiscount, latDiscount, latDiscount, discountamount,
                            toDateInLong, fromDateInLong, true);
                } else {
                    Toast.makeText(DiscountFormActivity.this, "Wrong format found", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<Discount> call, Throwable t) {
                Toast.makeText(DiscountFormActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void sendDiscount(final Integer shopkeeperId, Double latitude,
                             Double longitude, Integer discountAmount,
                             Long fromDate, Long toDate, Boolean isActive) {

        final Discount addDiscount = new Discount(shopkeeperId, latitude, longitude, discountAmount, fromDate, toDate, isActive);
        Call<Discount> call1 = apiServiceDiscountPost.addDiscountPost(addDiscount);
        call1.enqueue(new Callback<Discount>() {
            @Override
            public void onResponse(Call<Discount> call, Response<Discount> response) {
                if (!response.isSuccessful()) {

                    Toast.makeText(DiscountFormActivity.this, "Wrong format found", Toast.LENGTH_SHORT).show();
                }

                else {

                    showResponseDiscount(ADDED_DISCOUNT);
                }


            }

            @Override
            public void onFailure(Call<Discount> call, Throwable t) {




                Toast.makeText(DiscountFormActivity.this, "Failure message \n" + t.toString(), Toast.LENGTH_LONG).show();
            }
        });


    }

}



