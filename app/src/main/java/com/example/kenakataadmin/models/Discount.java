package com.example.kenakataadmin.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Discount {
    public Discount(Integer shopkeeperId, Double latitude,
                    Double longitude, Integer discountAmount,
                    Long fromDate, Long toDate, Boolean isActive) {
        this.shopkeeperId = shopkeeperId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.discountAmount = discountAmount;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.isActive = isActive;
    }

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ShopkeeperId")
    @Expose
    private Integer shopkeeperId;
    @SerializedName("Latitude")
    @Expose
    private Double latitude;
    @SerializedName("Longitude")
    @Expose
    private Double longitude;
    @SerializedName("DiscountAmount")
    @Expose
    private Integer discountAmount;
    @SerializedName("FromDate")
    @Expose
    private Long fromDate;
    @SerializedName("ToDate")
    @Expose
    private Long toDate;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopkeeperId() {
        return shopkeeperId;
    }

    public void setShopkeeperId(Integer shopkeeperId) {
        this.shopkeeperId = shopkeeperId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Long getFromDate() {
        return fromDate;
    }

    public void setFromDate(Long fromDate) {
        this.fromDate = fromDate;
    }

    public Long getToDate() {
        return toDate;
    }

    public void setToDate(Long toDate) {
        this.toDate = toDate;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
