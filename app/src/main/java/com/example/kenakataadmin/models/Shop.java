package com.example.kenakataadmin.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop {
    public Shop(String shopName, String shopType, String fullAddress,
                String ownerName, Double latitude,
                Double longitude, String imageURL, String phone, String email) {
        this.shopName = shopName;
        this.shopType = shopType;
        this.fullAddress = fullAddress;
        this.ownerName = ownerName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.imageURL = imageURL;
        this.phone = phone;
        this.email = email;
    }

    public Shop(Integer id, String shopName, String shopType,
                String fullAddress, String ownerName,
                Double latitude, Double longitude, String imageURL, String phone, String email) {
        this.id = id;
        this.shopName = shopName;
        this.shopType = shopType;
        this.fullAddress = fullAddress;
        this.ownerName = ownerName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.imageURL = imageURL;
        this.phone = phone;
        this.email = email;
    }

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ShopName")
    @Expose
    private String shopName;
    @SerializedName("ShopType")
    @Expose
    private String shopType;
    @SerializedName("FullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("OwnerName")
    @Expose
    private String ownerName;
    @SerializedName("Latitude")
    @Expose
    private Double latitude;
    @SerializedName("Longitude")
    @Expose
    private Double longitude;
    @SerializedName("ImageURL")
    @Expose
    private String imageURL;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Email")
    @Expose
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
