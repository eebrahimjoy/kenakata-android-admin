package com.example.kenakataadmin.models;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitInstance {
    public static final String BASE_URL = "http://kenakatalive71.somee.com/";
    private static final String BASE_URL_PLACE_API="https://maps.googleapis.com/maps/api/";
    private static Retrofit retrofit;
    private static Retrofit retrofitt;

    public static Retrofit getRetrofitInstanceForAPI(){

        if (retrofit==null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public static Retrofit getRetrofitInstanceForPlaceAPI(){

        if (retrofitt==null){
            retrofitt = new Retrofit.Builder().baseUrl(BASE_URL_PLACE_API).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofitt;
    }
    }