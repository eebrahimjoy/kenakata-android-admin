package com.example.kenakataadmin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kenakataadmin.models.Discount;
import com.example.kenakataadmin.models.RetrofitInstance;
import com.example.kenakataadmin.services.ApiService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDiscountActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    TextView toDateDiscountEdit, fromDateDiscountEdit;
    EditText addDiscountEdit;
    Button addAllEdit;
    TextView responseDiscountEdit;
    private long fromDateEdit, toDateEdit;
    int discountamountEditt;

    String toDateDiscountForm;
    String fromDateDiscountForm;

    //
    private boolean active;
    private ApiService editApiService;
    private boolean statusEdit;
    private double latitudeEdit, longitudeEdit;
    private int shopkeeperIdDiscount;

    private int shopkeeperIdDiscountEdit;
    private Snackbar snackbar;
    ApiService apiServiceAllDiscountList;
    private static final String DATA_SAVED = " Your data is saved successfully";


    private DatePickerDialog.OnDateSetListener dateSetListenerTo;
    private DatePickerDialog.OnDateSetListener dateSetListenerFrom;

    private long toDateInLong;
    private long fromDateInLong;
    ApiService apiServiceDiscountPost;
    TextView responseDiscount, toDateDiscount, fromDateDiscount;
    int idDiscount;
    double latDiscount, lonDiscount;
    private Spinner spinnerEdit;

    int discountIdDiscountForm, flag = 0;
    int discountAmountDiscountForm;

    boolean activeStatusDiscountForm;


    private static final String[] pathsEdit = {"Active", "Deactive"};


    String activeDiscountUrlEdit;
    ApiService editActiveApiServiceEdit;

    int discountIdForInactiveStatusEdit = -1;
    int discountIdForPositionEdit = 0;
    private int activeShopKeeperIdEdit;
    private int activeDiscountEdit;
    double activeLatEdit;
    double activeLotEdit;
    long activeFromDateEdit;
    long activeToDateEdit;
    int flagIntEdit = 0;
    private static final String DISCOUNT_UPDATED = " Discount update successfully";
    private static final int UPDATED_DISCOUNT = 2;


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            editBackRefresh();
            snackbar.dismiss();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                active = true;
                break;
            case 1:
                active = false;
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_discount);

        getSpinnerEdit(pathsEdit);


        addDiscountEdit = findViewById(R.id.addDiscountAmountEdit);
        fromDateDiscountEdit = findViewById(R.id.addFromDateEdit);
        toDateDiscountEdit = findViewById(R.id.addToDateEdit);

        addAllEdit = findViewById(R.id.addAllEditBtn);

        responseDiscountEdit = findViewById(R.id.responseDisountEditTV);
        editApiService = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);

        Bundle getExtraEdit = getIntent().getExtras();
        if (getExtraEdit != null) {
            discountIdDiscountForm = getExtraEdit.getInt("IdEdit");
            fromDateDiscountForm = getExtraEdit.getString("FromDate");
            toDateDiscountForm = getExtraEdit.getString("ToDate");
            discountAmountDiscountForm = getExtraEdit.getInt("Discount");
            activeStatusDiscountForm = getExtraEdit.getBoolean("ActiveStatus");
            shopkeeperIdDiscountEdit = getExtraEdit.getInt("ShopKeeperID");
            latitudeEdit = getExtraEdit.getDouble("LatEdit");
            longitudeEdit = getExtraEdit.getDouble("LongEdit");
        }
        // getActiveDiscountStatusForEdit(shopkeeperIdDiscountEdit);


        addDiscountEdit.setText(String.valueOf(discountAmountDiscountForm));
        toDateDiscountEdit.setText(String.valueOf(toDateDiscountForm));
        fromDateDiscountEdit.setText(String.valueOf(fromDateDiscountForm));


        addAllEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String discountamountedit = addDiscountEdit.getText().toString();
                String todate = toDateDiscountEdit.getText().toString();
                String fromdate = fromDateDiscountEdit.getText().toString();

                if (discountamountedit.equals(discountAmountDiscountForm) &&
                        todate.equals(toDateDiscountForm) &&
                        fromdate.equals(fromDateDiscountForm) &&
                        active == (activeStatusDiscountForm)) {

                    Toast.makeText(EditDiscountActivity.this, "No changed, You can't update", Toast.LENGTH_SHORT).show();
                } else {


                    DateFormat formatEdit = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date dateEdit = formatEdit.parse(todate);
                        fromDateEdit = dateEdit.getTime();

                        Date date1Edit = formatEdit.parse(fromdate);
                        toDateEdit = date1Edit.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    discountamountEditt = Integer.valueOf(discountamountedit);


                    if (active == false) {
                        editDiscount(shopkeeperIdDiscountEdit, latitudeEdit, longitudeEdit, discountamountEditt, fromDateEdit, toDateEdit, active);
                    } else if (active == true) {
                        getActiveDiscountStatusForEdit(shopkeeperIdDiscountEdit);

                    }


                }
            }
        });


        toDateDiscountEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendarTo = Calendar.getInstance();
                int yearTo = calendarTo.get(Calendar.YEAR);
                int monthTo = calendarTo.get(Calendar.MONTH);
                int dayTo = calendarTo.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        EditDiscountActivity.this, AlertDialog.THEME_HOLO_LIGHT, dateSetListenerTo, yearTo, monthTo, dayTo);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        fromDateDiscountEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendarFrom = Calendar.getInstance();
                int yearFrom = calendarFrom.get(Calendar.YEAR);
                int monthFrom = calendarFrom.get(Calendar.MONTH);
                int dayFrom = calendarFrom.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        EditDiscountActivity.this, AlertDialog.THEME_HOLO_LIGHT, dateSetListenerFrom, yearFrom, monthFrom, dayFrom);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        dateSetListenerTo = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                String date1 = day + "/" + month + "/" + year;
                toDateDiscountEdit.setText(date1);


            }
        };

        dateSetListenerFrom = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                String date = day + "/" + month + "/" + year;
                fromDateDiscountEdit.setText(date);


            }
        };
    }

    private void getSpinnerEdit(String[] pathsEdit) {
        spinnerEdit = (Spinner) findViewById(R.id.simpleSpinnerEdit);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(EditDiscountActivity.this,
                android.R.layout.simple_spinner_item, pathsEdit);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEdit.setAdapter(adapter);
        spinnerEdit.setOnItemSelectedListener(this);
    }


    public void getActiveDiscountStatusForEdit(int id) {
        activeDiscountUrlEdit = String.format("api/Discounts/AllDiscounts?id=%d", id);
        editActiveApiServiceEdit = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        final Call<List<Discount>> discountCallActive = editActiveApiServiceEdit.getAllDiscount(activeDiscountUrlEdit);
        discountCallActive.enqueue(new Callback<List<Discount>>() {
            @Override
            public void onResponse(Call<List<Discount>> call, Response<List<Discount>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(EditDiscountActivity.this, "Wrong format found", Toast.LENGTH_SHORT).show();

                } else {
                    final List<Discount> allDiscountss = response.body();


                    for (int i = 0; i < allDiscountss.size(); i++) {
                        statusEdit = allDiscountss.get(i).getIsActive();
                        if (allDiscountss.get(i).getIsActive().equals(true)) {
                            flagIntEdit = 1;
                            discountIdForPositionEdit = i;
                            discountIdForInactiveStatusEdit = allDiscountss.get(discountIdForPositionEdit).getId();
                            activeShopKeeperIdEdit = allDiscountss.get(discountIdForPositionEdit).getShopkeeperId();
                            activeDiscountEdit = allDiscountss.get(discountIdForPositionEdit).getDiscountAmount();
                            activeLatEdit = allDiscountss.get(discountIdForPositionEdit).getLatitude();
                            activeLotEdit = allDiscountss.get(discountIdForPositionEdit).getLongitude();
                            activeFromDateEdit = allDiscountss.get(discountIdForPositionEdit).getFromDate();
                            activeToDateEdit = allDiscountss.get(discountIdForPositionEdit).getToDate();

                            android.support.v7.app.AlertDialog.Builder builder =
                                    new android.support.v7.app.AlertDialog.Builder(EditDiscountActivity.this);
                            builder.setMessage("Already an active discount found, are you deactivate it??")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            inactiveStatusEdit(discountIdForInactiveStatusEdit,
                                                    activeShopKeeperIdEdit, activeLatEdit,
                                                    activeLotEdit, activeDiscountEdit, activeFromDateEdit,
                                                    activeToDateEdit, false);


                                        }
                                    }).setNegativeButton("No", null);
                            android.support.v7.app.AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }

                    }
                    if (flagIntEdit == 0) {
                        editDiscount(shopkeeperIdDiscountEdit, latitudeEdit, longitudeEdit, discountamountEditt, fromDateEdit, toDateEdit, active);

                    }


                }
            }

            @Override
            public void onFailure(Call<List<Discount>> call, Throwable t) {
                Toast.makeText(EditDiscountActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void editDiscount(Integer shopkeeperIdEdit, Double latitudeEdit,
                              Double longitudeEdit, Integer discountAmount,
                              Long fromDateEdit, Long toDateEdit, Boolean isActiveEdit) {

        final Discount discountEdit = new Discount(shopkeeperIdEdit, latitudeEdit, longitudeEdit, discountAmount, fromDateEdit,
                toDateEdit, isActiveEdit);
        Call<Discount> call1 = editApiService.editDiscount(discountIdDiscountForm, discountEdit);
        call1.enqueue(new Callback<Discount>() {
            @Override
            public void onResponse(Call<Discount> call, Response<Discount> response) {
                if (response.isSuccessful()) {
                    showResponseEdit();
                } else {
                    Toast.makeText(EditDiscountActivity.this, "Wrong format found", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<Discount> call, Throwable t) {
                Toast.makeText(EditDiscountActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void showResponseEdit() {
        snackbar = Snackbar.make(responseDiscountEdit, "" + DISCOUNT_UPDATED, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editBackRefresh();
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void editBackRefresh() {
        Intent returnEditIntent = new Intent();
        returnEditIntent.putExtra("ShopkeeperIdReturn", shopkeeperIdDiscountEdit);
        setResult(Activity.RESULT_OK, returnEditIntent);
        finish();
    }

    private void inactiveStatusEdit(Integer activeDiscountId, Integer shopkeeperEdit, Double latEdit,
                                    Double lonEdit, Integer disAmount,
                                    Long frdateEdit, Long toDatEdit, Boolean activeEdit) {
        editActiveApiServiceEdit = RetrofitInstance.getRetrofitInstanceForAPI().create(ApiService.class);
        final Discount discountActiveEdit = new Discount(shopkeeperEdit, latEdit, lonEdit,
                disAmount, frdateEdit, toDatEdit, activeEdit);
        Call<Discount> call1 = editActiveApiServiceEdit.editActiveDiscount(activeDiscountId, discountActiveEdit);
        call1.enqueue(new Callback<Discount>() {
            @Override
            public void onResponse(Call<Discount> call, Response<Discount> response) {
                if (response.isSuccessful()) {
                    editDiscount(shopkeeperIdDiscountEdit, latitudeEdit, longitudeEdit, discountamountEditt, fromDateEdit, toDateEdit, active);
                } else {
                    Toast.makeText(EditDiscountActivity.this, "Wrong format found", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<Discount> call, Throwable t) {
                Toast.makeText(EditDiscountActivity.this, "Connect the device with internet", Toast.LENGTH_SHORT).show();

            }
        });


    }


}



